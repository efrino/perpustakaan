<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tampil Data Buku</title>
</head>
<body>
	<?php  
		include'config.php';
		$db = new Database();
	?>
	<table border="1">
	<tr>
		<th>No</th>
		<th>Kode Buku</th>
		<th>Judul Buku</th>
		<th>Pengarang</th>
		<th>Jenis Buku</th>
		<th>Penerbit</th>
		<th>ISBN</th>
		<th>Tahun</th>
		<th>Deskripsi</th>
		<th>Jumlah</th>
		<th>Edit</th>
		<th>Hapus</th>
	</tr>
	<?php  
	$no = 1;
	foreach($db->tampil_data_buku() as $x){
	?>
	<tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo $x['kode_buku']; ?></td>
		<td><?php echo $x['judul_buku']; ?></td>
		<td><?php echo $x['nama_pengarang']; ?></td>
		<td><?php echo $x['nama_jenis_buku']; ?></td>
		<td><?php echo $x['nama_penerbit']; ?></td>
		<td><?php echo $x['isbn']; ?></td>
		<td><?php echo $x['tahun']; ?></td>
		<td><?php echo $x['deskripsi']; ?></td>
		<td><?php echo $x['jumlah']; ?></td>
		<td><a href="edit_data_buku.php?id=<?php echo $x['kode_buku']; ?>">Edit</a></td>
		<td><a href="hapus_data_buku.php?id=<?php echo $x['kode_buku']; ?>">Hapus</a></td>
	</tr>
	<?php  
	}
	?>
	</table>
	<div>
		<a href="tambah_data_buku.php">Tambah Data Buku</a>
		<a href="index.php">Home</a>
	</div>
</body>
</html>