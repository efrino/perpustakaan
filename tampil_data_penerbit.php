<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tampil Data Penerbit</title>
</head>
<body>
	<?php  
		include'config.php';
		$db = new Database();
	?>
	<table border="1">
	<tr>
		<th>No</th>
		<th>Kode Penerbit</th>
		<th>Nama Penerbit</th>
		<th>Edit</th>
		<th>Hapus</th>
	</tr>
	<?php  
	$no = 1;
	foreach($db->tampil_data_penerbit() as $x){
	?>
	<tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo $x['kode_penerbit']; ?></td>
		<td><?php echo $x['nama_penerbit']; ?></td>
		<td><a href="edit_data_penerbit.php?id=<?php echo $x['kode_penerbit']; ?>">Edit</a></td>
		<td><a href="hapus_data_penerbit.php?id=<?php echo $x['kode_penerbit']; ?>">Hapus</a></td>
	</tr>
	<?php  
	}
	?>
	</table>
	<div>
		<a href="tambah_data_penerbit.php">Tambah Data Penerbit</a>
		<a href="index.php">Home</a>
	</div>
</body>
</html>