<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Tampil Data Jenis Buku</title>
</head>
<body>
	<?php  
		include'config.php';
		$db = new Database();
	?>
	<table border="1">
	<tr>
		<th>No</th>
		<th>Kode Jenis Buku</th>
		<th>Nama Jenis Buku</th>
		<th>Edit</th>
		<th>Hapus</th>
	</tr>
	<?php  
	$no = 1;
	foreach($db->tampil_data_jenis_buku() as $x){
	?>
	<tr>
		<td><?php echo $no++; ?></td>
		<td><?php echo $x['kode_jenis_buku']; ?></td>
		<td><?php echo $x['nama_jenis_buku']; ?></td>
		<td><a href="edit_data_jenis_buku.php?id=<?php echo $x['kode_jenis_buku']; ?>">Edit</a></td>
		<td><a href="hapus_data_jenis_buku.php?id=<?php echo $x['kode_jenis_buku']; ?>">Hapus</a></td>
	</tr>
	<?php  
	}
	?>
	</table>
	<div>
		<a href="tambah_data_jenis_buku.php">Tambah Data Jenis Buku</a>
		<a href="index.php">Home</a>
	</div>
</body>
</html>